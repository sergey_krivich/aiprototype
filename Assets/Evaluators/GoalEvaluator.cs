﻿using Assets;
using UnityEngine;

public abstract class GoalEvaluator
{
  public abstract float Evaluate(Bot owner);
  public abstract Goal GetGoal(Bot owner);
}
