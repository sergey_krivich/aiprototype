﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Evaluators
{
  public class ChangeBeaconEvaluator:GoalEvaluator
  {
    public override float Evaluate(Bot owner)
    {
      if (owner.BeaconToCapture.Beacon == null)
        return 1.0f;

      if (owner.BeaconToCapture.Beacon.CapturedBy == owner.Team)
        return 1.0f;

      return 0.0f;

      Beacon closestBeacon = null;
      float minDistance = float.MaxValue;
      foreach (var beacon in Beacon.All)
      {
        var distance = Vector3.Distance(
          owner.transform.position,
          owner.BeaconToCapture.Beacon.transform.position);
        if (distance < minDistance)
        {
          minDistance = distance;
          closestBeacon = beacon;
        }
      }

      if (closestBeacon != owner.BeaconToCapture.Beacon)
      {
        var closest = GetClosestActor(closestBeacon, owner.Team);
        if (closest == owner)
        {
          return 1.0f;
        }
      }

      var beaconDanger = TileMap.I.GetDangerSum(owner, owner.BeaconToCapture.Beacon.transform.position, 1);
      if (beaconDanger > 10f)
      {
        return beaconDanger/10f;
      }

      var closestActor = GetClosestActor(owner.BeaconToCapture.Beacon, owner.Team);
      if (closestActor != owner)
      {
        var beaconHeading = owner.GetBeaconHeading(owner.BeaconToCapture.Beacon);
        return beaconHeading;
      }

      return 0.0f;
    }

    private static Bot GetClosestActor(Beacon beacon, TeamId team)
    {
      Bot closestActor = null;
      float minDistance = float.MaxValue;
      foreach (var bot in Bot.All)
      {
        if(bot.Team != team)
          continue;

        var distance = Vector3.Distance(
          bot.transform.position,
          beacon.transform.position);
        if (distance < minDistance)
        {
          minDistance = distance;
          closestActor = bot;
        }
      }
      return closestActor;
    }

    public override Goal GetGoal(Bot owner)
    {
      return new GoalChangeBeaconToCapture(owner);
    }
  }
}
