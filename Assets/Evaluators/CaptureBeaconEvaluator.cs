﻿using Assets;

public class CaptureBeaconEvaluator : GoalEvaluator
{
  public override float Evaluate(Bot owner)
  {
    if (owner.BeaconToCapture.Beacon == null)
      return 0f;
    if (owner.BeaconToCapture.Beacon.CapturedBy == owner.Team)
      return 0.0f;



    return 0.5f;
  }

  public override Goal GetGoal(Bot owner)
  {
    return new GoalCaptureBeacon(owner);
  }
}
