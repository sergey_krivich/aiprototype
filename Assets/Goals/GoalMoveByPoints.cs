﻿using System.Collections.Generic;
using Assets;
using UnityEngine;

public class GoalMoveByPoints : Goal
{
  private readonly Vector3 _targetPos;
  private List<Vector3> _points;

  public GoalMoveByPoints(Bot owner, Vector3 targetPos) : base(owner)
  {
    _targetPos = targetPos;
  }

  protected override void OnActivate()
  {
    _points = TileMap.I.FindPath(Owner, Owner.transform.position, _targetPos);

    foreach (var p in _points)
    {
      AddSubgoal(new GoalMoveToPoint(Owner, p, 10f));
    }
  }

  public override GoalStatus OnUpdate()
  {
    for (int i = 0; i < _points.Count - 1; i++)
    {
      Debug.DrawLine(_points[i], _points[i + 1], Owner.Team == TeamId.A ? Color.blue : Color.red);
    }

    return base.OnUpdate();
  }
}
