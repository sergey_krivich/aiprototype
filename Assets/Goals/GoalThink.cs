﻿using System.Collections.Generic;
using System.Linq;
using Assets;
using UnityEngine;

public class GoalThink : Goal
{
  private readonly List<GoalEvaluator> _evaluators;

  private Goal _currentGoal;

  public GoalThink(Bot owner, params GoalEvaluator[] evaluators)
    :base(owner)
  {
    _evaluators = evaluators.ToList();
  }

  public override GoalStatus OnUpdate()
  {
    GoalEvaluator maxEv = null;
    var maxEvValue = 0f;



    foreach (var goalEvaluator in _evaluators)
    {
      var value = Mathf.Clamp01(goalEvaluator.Evaluate(Owner));

      if (value > maxEvValue)
      {
        maxEv = goalEvaluator;
        maxEvValue = value;
      }

      Owner.BotDebug += "Ev " + goalEvaluator.GetType().Name + " V:" + value + "\n";
    }

    if (maxEv == null)
    {
      Debug.LogError("Something");
    }

    var newGoal = maxEv.GetGoal(Owner);

    if (_currentGoal == null || newGoal.GetType() != _currentGoal.GetType() || _currentGoal.Status == GoalStatus.Completed)
    {
      if (_currentGoal != null)
      {
        _currentGoal.Deactivate();
      }

      _currentGoal = newGoal;
      _currentGoal.Activate();
    }

    Owner.BotDebug += "Current goal " + _currentGoal.GetType().Name +"\n";
    _currentGoal.Update();

    return GoalStatus.Active;
  }
}
