﻿namespace Assets.Evaluators
{
  public class GoalChangeBeaconToCapture:Goal
  {
    public GoalChangeBeaconToCapture(Bot owner) 
      : base(owner)
    {
    }

    protected override void OnActivate()
    {
      Owner.TryChangeBeacon();
    }
  }
}
