﻿using Assets;
using UnityEngine;
using UnityEngine.AI;

public class GoalMoveToPoint : Goal
{
  private readonly Vector3 _point;
  private readonly float _stopDistance;

  public GoalMoveToPoint(Bot owner, Vector3 point, float stopDistance = 5f) 
    : base(owner)
  {
    _point = point;
    _stopDistance = stopDistance;
  }

  protected override void OnActivate()
  {
    base.OnActivate();

    var agent = Owner.GetComponent<NavMeshAgent>();
    agent.SetDestination(_point);
  }

  public override GoalStatus OnUpdate()
  {
    if (Vector3.Distance(Owner.transform.position, _point) < _stopDistance)
    {
      return GoalStatus.Completed;
    }

    return GoalStatus.Active;
  }
}
