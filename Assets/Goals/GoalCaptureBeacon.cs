﻿using System.Linq;
using Assets;
using UnityEngine;

public class GoalCaptureBeacon : Goal
{
  private Bot.ScoredBeacon _targetBeacon;

  public GoalCaptureBeacon(Bot owner) 
    : base(owner)
  {
  }

  protected override void OnActivate()
  {
    base.OnActivate();

    _targetBeacon = Owner.BeaconToCapture;

    if (_targetBeacon.Beacon != null)
      AddSubgoal(new GoalMoveByPoints(Owner, _targetBeacon.Beacon.transform.position));
  }

  public override GoalStatus OnUpdate()
  {
    if(_targetBeacon.Beacon != null)
      Debug.DrawLine(Owner.transform.position, _targetBeacon.Beacon.transform.position);

    if (ShouldChangeBeacon() > 0.5f)
    {
      Owner.TryChangeBeacon();
    }

    var targetBeacon = Owner.BeaconToCapture;
    if(_targetBeacon.Beacon != targetBeacon.Beacon)
      return GoalStatus.Completed;

    return base.OnUpdate();
  }

  private float ShouldChangeBeacon()
  {
    if (Owner.BeaconToCapture.Beacon == null)
      return 1.0f;

    if (Owner.BeaconToCapture.Beacon.CapturedBy == Owner.Team)
      return 1.0f;

    Beacon closestBeacon = null;
    float minDistance = float.MaxValue;
    foreach (var beacon in Beacon.All)
    {
      var distance = Vector3.Distance(
        Owner.transform.position,
        Owner.BeaconToCapture.Beacon.transform.position);
      if (distance < minDistance)
      {
        minDistance = distance;
        closestBeacon = beacon;
      }
    }

    if (closestBeacon != Owner.BeaconToCapture.Beacon)
    {
      var closest = GetClosestActor(closestBeacon, Owner.Team);
      if (closest == Owner)
      {
        return 1.0f;
      }
    }

    var beaconDanger = TileMap.I.GetDangerSum(Owner, Owner.BeaconToCapture.Beacon.transform.position, 1);
    if (beaconDanger > 5)
    {
      Debug.Log("changed because of danger");
      return beaconDanger / 5;
    }

    var closestActor = GetClosestActor(Owner.BeaconToCapture.Beacon, Owner.Team);
    if (closestActor != Owner)
    {
      var beaconHeading = Owner.GetBeaconHeading(Owner.BeaconToCapture.Beacon);
      return beaconHeading;
    }

    return 0.0f;
  }

  private static Bot GetClosestActor(Beacon beacon, TeamId team)
  {
    Bot closestActor = null;
    float minDistance = float.MaxValue;
    foreach (var bot in Bot.All)
    {
      if (bot.Team != team)
        continue;

      var distance = Vector3.Distance(
        bot.transform.position,
        beacon.transform.position);
      if (distance < minDistance)
      {
        minDistance = distance;
        closestActor = bot;
      }
    }
    return closestActor;
  }
}
