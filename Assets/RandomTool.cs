﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

public class RandomTool
{
  /// <summary>
  /// Determenistic _random for game logic
  /// </summary>
  public static readonly RandomTool D = new RandomTool(1);

  /// <summary>
  /// Random for calls inside Update to shake camera, _random directional movement etc..
  /// </summary>
  public static readonly RandomTool U = new RandomTool();

  private readonly Random _random;

  public RandomTool()
  {
    _random = new Random();
  }

  public RandomTool(int seed)
  {
    _random = new Random(seed);
  }


  public int NextInt(int min, int max)
  {
    if (min < 0)
    {
      return _random.Next(0, min + max) - min;
    }

    return _random.Next(min, max);
  }

  public int NextInt(int max)
  {
    if (max >= 0)
      return _random.Next(max);
    return -12; //quite _random, I think
  }

  public int NextInt()
  {
    return _random.Next();
  }

  public T NextChoice<T>(List<T> objects)
  {
    if (objects.Count == 0)
    {
      return default(T);
    }

    return objects[NextInt(objects.Count)];
  }

  public T NextChoice<T>(T[] objects)
  {
    if (objects.Length == 0)
    {
      return default(T);
    }

    return objects[NextInt(objects.Length)];
  }

  public List<T> ChooseItems<T>(List<T> objects, int count)
  {
    var items = objects.ToList();

    while (items.Count - 1 > count)
    {
      items.RemoveAt(NextInt(items.Count));
    }

    return items;
  }

  public T NextWeightedChoice<T>(List<T> objects, List<float> chance)
  {
    if(objects.Count != chance.Count)
    {
      throw new ArgumentException("Objects and chance count must be the same: " + objects.Count + ", " + chance.Count);
    }

    if (objects.Count == 0)
    {
      return default(T);
    }

    float sumOfWeight = 0;
    for(int i=0; i<chance.Count; i++) {
      sumOfWeight += chance[i];
    }

    float rnd = NextSingle(0f, sumOfWeight);
    for(int i=0; i<chance.Count; i++) {
      if(rnd < chance[i])
        return objects[i];
      rnd -= chance[i];
    }

    //should never get here
    return objects[NextInt(objects.Count)];
  }


  public byte NextByte()
  {
    return (byte) _random.Next();
  }

  public byte NextByte(byte max)
  {
    return (byte) _random.Next(max);
  }

  public byte NextByte(byte min, byte max)
  {
    return (byte) _random.Next(min, max);
  }

  public double NextDouble()
  {
    return _random.NextDouble();
  }

  public float NextSingle()
  {
    return (float) _random.NextDouble();
  }

  public float NextSingle(float min, float max)
  {
    return (max - min)*NextSingle() + min;
  }

  public bool NextBool(float ratio)
  {
    return _random.NextDouble() <= ratio;
  }

  public bool NextBool(double ratio)
  {
    return _random.NextDouble() <= ratio;
  }

  public bool NextBool()
  {
    return _random.NextDouble() <= 0.5;
  }

  public sbyte NextSign()
  {
    return _random.NextDouble() <= 0.5 ? (sbyte) 1 : (sbyte) -1;
  }

  public Vector2 RandomDir2D()
  {
    if(NextBool())
      return new Vector2(NextSign(), 0);

    return new Vector2(0, NextSign());
  }


  public Color NextColor()
  {
    return new Color(NextByte()/255f, NextByte()/255f, NextByte()/255f, 1f);
  }

  public Vector2 NextUnitVector2()
  {
    float radians = NextSingle(-Mathf.PI, Mathf.PI);
    return new Vector2((float) Math.Cos(radians), (float) Math.Sin(radians));
  }

  public Vector3 NextUnitVector3()
  {
    Single radians = NextSingle(-Mathf.PI, Mathf.PI);

    Single z = NextSingle(-1f, 1f);

    Single t = (float) Math.Sqrt(1f - (z*z));

    Vector2 planar = new Vector2
    {
      x = (float) Math.Cos(radians)*t,
      y = (float) Math.Sin(radians)*t
    };

    return new Vector3
    {
      x = planar.x,
      y = planar.y,
      z = z
    };
  }
}
