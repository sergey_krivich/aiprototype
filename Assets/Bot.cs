﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Evaluators;
using UnityEngine;

public enum TeamId
{
  Undefined,
  A,
  B,
  All
}

public class Bot : MonoBehaviour
{
  public static List<Bot> All = new List<Bot>();
  public TeamId Team;
  private GoalThink _think;

  [Serializable]
  public class BotSettings
  {
    public float DangerPathAvoidance = 2f;
    public float BeaconDangerAvoidance = 2f;
    public float SameHeadingAvoidance = 2;

    public float BeaconDistance = 0.05f;
  }

  public struct ScoredBeacon
  {
    public float Score;
    public Beacon Beacon;
  }

  [SerializeField]
  public BotSettings Settings;

  public float HP = 100;
  public string BotDebug;

  public List<ScoredBeacon> RatedBeacons { get; set; }

  public Vector3 PreviousPosition;

  public ScoredBeacon BeaconToCapture;

  public GameObject Bullet;

  private Vector3 _spawnPOint;
  void Awake()
  {
    All.Add(this);
    _spawnPOint = transform.position;
    PreviousPosition = _spawnPOint;
  }

  void Start()
  {
    _think = new GoalThink(this,
      new CaptureBeaconEvaluator(),
      new ChangeBeaconEvaluator());

    var mat = GetComponent<MeshRenderer>().material;
    var color = Color.black;

    if (Team == TeamId.A)
    {
      color = Color.blue;
    }
    else if (Team == TeamId.B)
    {
      color = Color.red;
    }

    mat.SetColor("_Color", color);
    GetComponent<MeshRenderer>().material = mat;
  }

  void Update()
  {
    BotDebug = "";
    RatedBeacons = RateBeacons();
    foreach (var scoredBeacon in RatedBeacons)
    {
      BotDebug += "B " + scoredBeacon.Beacon.name + " s: " + scoredBeacon.Score + "\n";
    }

    _think.Update();

    UpdateShooting();

    PreviousPosition = Vector3.Lerp(PreviousPosition, transform.position, Time.deltaTime*0.5f);
  }

  private float _shootelapsed;

  public void TryChangeBeacon()
  {
    BeaconToCapture = RatedBeacons.FirstOrDefault();
  }

  private void UpdateShooting()
  {
    _shootelapsed -= Time.deltaTime;

    Bot closestBot = null;
    float minDistance = float.MaxValue;
    foreach (var bot in All)
    {
      if (bot.Team == Team)
        continue;

      float distance = Vector3.Distance(bot.transform.position, transform.position);

      if (distance < minDistance)
      {
        closestBot = bot;
        minDistance = distance;
      }
    }


    if (closestBot != null)
    {
      var distance = Vector3.Distance(closestBot.transform.position, transform.position);
      const float shootDistance = 20;
      if (distance < shootDistance)
      {
        closestBot.TakeDamage(Time.deltaTime*4*(1f - distance/shootDistance));
        if (_shootelapsed <= 0)
        {
          _shootelapsed = 0.4f;
          var bullet = GameObject.Instantiate(Bullet, transform.position, Quaternion.identity);
          bullet.GetComponent<Bullet>().SetTarget(closestBot);
          Destroy(bullet, 3f);
        }
      }
    }
  }

  private void TakeDamage(float damage)
  {
    HP -= damage;
    if (HP <= 0)
    {
      transform.position = _spawnPOint;
      HP = 100;
      PreviousPosition = _spawnPOint;
    }
  }

  private List<ScoredBeacon> RateBeacons()
  {
    var scored = new List<ScoredBeacon>();

    foreach (var beacon in Beacon.GetNonCaptured(Team))
    {
      var score = 0f;
      score += Vector3.Distance(transform.position, beacon.transform.position)* Settings.BeaconDistance;
      score += TileMap.I.GetDangerSum(this, beacon.transform.position, 3)*Settings.BeaconDangerAvoidance;
      score += GetBeaconHeading(beacon);

      var scoredBeacon = new ScoredBeacon();
      scoredBeacon.Score = score;
      scoredBeacon.Beacon = beacon;

      scored.Add(scoredBeacon);
    }

    return scored.OrderBy(s => s.Score).ToList();
  }

  public float GetBeaconHeading(Beacon beacon)
  {
    float score = 0f;
    var heading = Team == TeamId.A ? beacon.AlliesHeadingHereA : beacon.AlliesHeadingHereB;
    score += (heading - beacon.GetHeading(this)) * Settings.SameHeadingAvoidance;
    return score;
  }

  void OnDrawGizmos()
  {
    Gizmos.color = new Color(1,1,1, 0.5f);
    Gizmos.DrawCube(PreviousPosition, Vector3.one);
  }
}
