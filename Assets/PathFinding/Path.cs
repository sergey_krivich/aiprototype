﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameName1.PathFinding
{
    public class Path
    {
        private readonly List<Vector3> points;
        public List<Vector3> ControlPoints { get { return points; } }

        public bool Finished
        {
            get { return !ControlPoints.Any(); }
        }

        public Path()
        {
            points = new List<Vector3>();
        }

        public float Length()
        {
            float l = 0;
            for (int i = 0; i < ControlPoints.Count - 1; i++)
            {
                l += (ControlPoints[i] - ControlPoints[i + 1]).magnitude;
            }
            return l;
        }

        public void AddPoints(List<Vector3> points)
        {
            this.points.AddRange(points);
        }
        public void AddPoint(Vector3 point)
        {
            points.Add(point);
        }
        public void InsertPoint(int index, Vector3 point)
        {
            points.Insert(index, point);
        }

        public void RemoveControlPoint(int index)
        {
            ControlPoints.RemoveAt(index);
        }

        public void Clear()
        {
            points.Clear();
            ControlPoints.Clear();
        }
    }
}
