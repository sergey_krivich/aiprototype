﻿using System;

namespace GameName1.PathFinding
{
  internal sealed class WayPoint
  {
    public readonly WayPoint Parent;

    public float G;
    public float H;

    public float Cost { get; private set; }

    public int PositionX;
    public int PositionY;

    public bool type;

    public WayPoint(Point Position, WayPoint Parent, bool type)
    {
      this.PositionX = Position.X;
      this.PositionY = Position.Y;
      this.Parent = Parent;
      this.type = type;
      H = 0;
    }

    public void CalculateCost(TileMap tileMap, Point endPoint, byte additionalCost, bool improved, Func<TileMap.Tile, float> tileCostAdder)
    {
      //H = Math.Max(Math.Abs(Position.X - endPoint.X), Math.Abs(Position.Y - endPoint.Y)) * 30;
      //   H = (Math.Abs(Position.X - endPoint.X) + Math.Abs(Position.Y - endPoint.Y))*10;
      //H = Math.Abs(Position.X - endPoint.X) + Math.Abs(Position.Y - endPoint.Y);
//            if(false)
//            if (!improved)
//            {
//                int xDistance = Math.Abs(PositionX - endPoint.X);
//                int yDistance = Math.Abs(PositionY - endPoint.Y);
//                if (xDistance > yDistance)
//                    H = 14 * yDistance + 10 * (xDistance - yDistance);
//                else
//                    H = 14 * xDistance + 10 * (yDistance - xDistance);
//            }
//            else
//                H = 0;

      //   H = (Math.Abs(PositionX - endPoint.X) - Math.Abs(PositionY - endPoint.Y)) * 10;

      if (Parent != null)
        if (type)
          G = Parent.G + 1f;
        else
          G = Parent.G + 1.4f;
      else
        G = 0;

      if (tileCostAdder != null)
        G += tileCostAdder(tileMap.Map[PositionX, PositionY]);
      Cost = G + H; //+ (tileMap[PositionX, PositionY] != CellType.Ladder ? 10000 : 0) + additionalCost
      // + (tileMap[PositionX, PositionY + 1] == CellType.Wall ? 0 : 5000);


      //  if (CreatureActor != null)
      //   Cost += (int)(100 / CreatureActor.Body.GetWalkSpeed(CreatureActor.Map.terrain[PositionX, PositionY]));
    }
  }
}