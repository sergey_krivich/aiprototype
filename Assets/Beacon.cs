﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beacon : MonoBehaviour
{
  public static List<Beacon> All = new List<Beacon>();
  public float CaptureStatus;

  public float AlliesHeadingHereA = 0f;
  public float AlliesHeadingHereB = 0f;


  public TeamId CapturedBy = TeamId.Undefined;

  //public TeamId CapturedBy
  //{
  //  get
  //  {
  //    if(CaptureStatus >= 1) return TeamId.A;
  //    if(CaptureStatus <= -1) return TeamId.B;

  //    return TeamId.Undefined;
  //  }
  //}


	void Awake()
  {
    All.Add(this);
	}

  public static IEnumerable<Beacon> GetNonCaptured(TeamId team)
  {
    foreach (var beacon in All)
    {
      if (beacon.CapturedBy != team)
      {
        yield return beacon;
      }
    }
  }
	
	void Update ()
	{
	  UpdateCapture();
	  RecalculateStatus();
	}

  public void RecalculateStatus()
  {
    AlliesHeadingHereA = 0f;
    AlliesHeadingHereB = 0f;

    foreach (var bot in Bot.All)
    {
      var result = GetHeading(bot);

      if (bot.Team == TeamId.A)
        AlliesHeadingHereA += result;
      else if (bot.Team == TeamId.B)
        AlliesHeadingHereB += result;
    }
  }

  public float GetHeading(Bot bot)
  {
    var botsMovementDir = (bot.transform.position - bot.PreviousPosition).normalized;
    var botsDirectionToBeacon = (transform.position - bot.transform.position).normalized;
    var dot = Vector3.Dot(botsMovementDir, botsDirectionToBeacon) /
              Mathf.Sqrt(Vector3.Distance(bot.transform.position, transform.position));

    var result = 0f;

    if (dot > 0)
    {
      result += dot;
    }
    return result;
  }

  public float CaptureSpeed = 0.3f;

  private void UpdateCapture()
  {
    var capturersA = 0;
    var capturersB = 0;

    foreach (var bot in Bot.All)
    {
      if (Vector3.Distance(bot.transform.position, transform.position) < 8)
      {
        if (bot.Team == TeamId.A)
        {
          capturersA++;
        }
        else
        {
          capturersB++;
        }
      }
    }

    if (capturersA != 0 && capturersB == 0)
    {
      CaptureStatus += Time.deltaTime* CaptureSpeed;
    }
    else if (capturersB != 0 && capturersA == 0)
    {
      CaptureStatus -= Time.deltaTime* CaptureSpeed;
    }

    CaptureStatus = Mathf.Clamp(CaptureStatus, -1, 1);

    if (CaptureStatus >= 1)
    {
      CapturedBy = TeamId.A;
    }
    else if(CaptureStatus <= -1)
    {
      CapturedBy = TeamId.B;
    }
    else
    {
      if (CapturedBy == TeamId.A && CaptureStatus > 0 && capturersB == 0)
      {
        CaptureStatus += Time.deltaTime* CaptureSpeed;
      }
      else if (CapturedBy == TeamId.B && CaptureStatus < 0 && capturersA == 0)
      {
        CaptureStatus -= Time.deltaTime* CaptureSpeed;
      }
      else
      {
        if(capturersB == 0&& capturersA == 0)
          CaptureStatus = Mathf.Lerp(CaptureStatus, 0, Time.deltaTime);

        CapturedBy = TeamId.Undefined;
      }
    }

    CaptureStatus = Mathf.Clamp(CaptureStatus, -1, 1);

    var mat = GetComponent<MeshRenderer>().material;


    var color = Color.black;

    if (CapturedBy == TeamId.A)
    {
      color = Color.blue;
    }
    else if (CapturedBy == TeamId.B)
    {
      color = Color.red;
    }

    mat.SetColor("_Color", color);
    GetComponent<MeshRenderer>().material = mat;
  }
}
