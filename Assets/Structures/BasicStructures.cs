﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

[StructLayout(LayoutKind.Sequential)]
[Serializable]
public struct Point
{
  public int X, Y;

  public Point(int x, int y)
  {
    this.X = x;
    this.Y = y;
  }

  public Point(int value)
  {
    this.X = value;
    this.Y = value;
  }


  public Point(Vector2 vec)
  {
    this.X = Mathf.RoundToInt(vec.x);
    this.Y = Mathf.RoundToInt(vec.y);
  }

  public float Distance(Point point)
  {
    return Vector2.Distance(new Vector2(point.X, point.Y), new Vector2(X, Y));
  }

  public static Point operator +(Point p1, Point p2)
  {
    return new Point(p1.X + p2.X, p1.Y + p2.Y);
  }

  public static Point operator -(Point p1, Point p2)
  {
    return new Point(p1.X - p2.X, p1.Y - p2.Y);
  }

  public static Point operator *(Point p1, int v)
  {
    return new Point(p1.X * v, p1.Y * v);
  }

  public static Point operator *(int v, Point p1)
  {
    return new Point(p1.X * v, p1.Y * v);
  }


  public static bool operator ==(Point p1, Point p2)
  {
    return p1.X == p2.X && p1.Y == p2.Y;
  }

  public static bool operator !=(Point p1, Point p2)
  {
    return p1.X != p2.X || p1.Y != p2.Y;
  }

  public override int GetHashCode()
  {
    return base.GetHashCode();
  }

  public override bool Equals(object obj)
  {
    return base.Equals(obj);
  }

  public static Point Empty
  {
    get { return new Point(0); }
  }

  public static Point Zero
  {
    get { return new Point(0); }
  }

  public float Magnitude
  {
    get { return Mathf.Sqrt(X * X + Y * Y); }
  }

  public override string ToString()
  {
    return string.Format("X:{0} Y:{1}", X, Y);
  }
}

public class Tuple<A, B>
{
  public A a;
  public B b;

  public Tuple(A a, B b)
  {
    this.a = a;
    this.b = b;
  }
}
