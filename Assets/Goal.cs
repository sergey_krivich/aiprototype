﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets
{
  public enum GoalStatus
  {
    Active,
    Completed,
    Inactive
  }

  public class Goal
  {
    public Bot Owner { get; private set; }
    public GoalStatus Status;
    private readonly List<Goal> _subGoals;

    public Goal(Bot owner)
    {
      Owner = owner;
      Status = GoalStatus.Inactive;
      _subGoals = new List<Goal>();
    }

    public virtual void AddSubgoal(Goal subgoal)
    {
      _subGoals.Add(subgoal);
    }

    public void Activate()
    {
      OnActivate();
    }

    public void Deactivate()
    {
      OnDeactivate();
    }

    protected virtual void OnActivate()
    {

    }

    protected virtual void OnDeactivate()
    {
      
    }

    public void Update()
    {
      Owner.BotDebug += "Goal: " + GetType().Name +"\n";

      Status = OnUpdate();
      var currentSubgoal = _subGoals.FirstOrDefault();
      if (currentSubgoal != null)
      {
        if (currentSubgoal.Status == GoalStatus.Inactive)
        {
          currentSubgoal.Activate();
        }

        currentSubgoal.Update();

        if (currentSubgoal.Status == GoalStatus.Completed)
        {
          _subGoals.RemoveAt(0);
        }
      }
    }

    public virtual GoalStatus OnUpdate()
    {
      return SubgoalStatus();
    }

    public GoalStatus SubgoalStatus()
    {
      return _subGoals.Count == 0 ? GoalStatus.Completed : GoalStatus.Active;
    }
  }
}
