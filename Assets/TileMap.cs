﻿using System;
using System.Collections.Generic;
using GameName1.PathFinding;
using UnityEngine;
using UnityEngine.AI;

public class TileMap : MonoBehaviour
{
  public float TileSize;
  public float Width;
  public float Height;

  public int TileWidth;
  public int TileHeight;

  public int BotUpdateRadius;
  public TeamId ToDisplay;

  public struct Tile
  {
    public float DangerTeamA;
    public float DangerTeamB;

    public float GetDanger(TeamId botTeam)
    {
      return botTeam == TeamId.A ? DangerTeamA : DangerTeamB;
    }
    public float GetOppositeDanger(TeamId botTeam)
    {
      return botTeam == TeamId.B ? DangerTeamA : DangerTeamB;
    }
  }

  public Tile[,] Map;
  public static TileMap I;
  void Awake()
  {
    I = this;
    Init();
  }

  public void Init()
  {
    TileWidth = (int) (Width / TileSize);
    TileHeight = (int) (Height / TileSize);
    Map = new Tile[TileWidth, TileHeight];
  }

  public void Reset()
  {
    Array.Clear(Map, 0, Map.Length);
  }

  public void PlaceBot(Bot bot)
  {
    var center = ToTile(bot.transform.position);
    for (int i = center.X - BotUpdateRadius; i <= center.X + BotUpdateRadius; i++)
    {
      for (int j = center.Y - BotUpdateRadius; j <= center.Y + BotUpdateRadius; j++)
      {
        if(!InRange(i,j))
          continue;

        var tileWorld = ToPositino(new Point(i, j));
        var distance = Vector3.Distance(tileWorld, bot.transform.position);
        var danger = 1f - distance/(TileSize*BotUpdateRadius);

        if (bot.Team == TeamId.A)
        {
          Map[i, j].DangerTeamA += danger;
        }
        else if (bot.Team == TeamId.B)
        {
          Map[i, j].DangerTeamB += danger;
        }
      }
    }
  }

  public List<Vector3> FindPath(Bot bot, Vector3 start, Vector3 end)
  {
    var startTile = ToTile(start);
    var endTile = ToTile(end);

    var astar = new AStar(this, t =>
    {
      var result = bot.Team == TeamId.B ? t.DangerTeamA : t.DangerTeamB;
      result -= bot.Team == TeamId.A ? t.DangerTeamA : t.DangerTeamB;

      if (result < 0)
        result = 0f;

      result *= bot.Settings.DangerPathAvoidance;
      return result;
    });

    astar.FindPath(startTile, endTile);
    var points = astar.Path.ControlPoints;
    points.Add(end);

    return points;
  }

  public float GetDangerSum(Bot bot, Vector3 position, int radius)
  {
    var tileCenter = ToTile(position);
    float sum = 0f;
    for (int i = tileCenter.X - radius; i <= tileCenter.X + radius; i++)
    {
      for (int j = tileCenter.Y - radius; j <= tileCenter.Y + radius; j++)
      {
        if(!InRange(i,j))
          continue;

        sum += Map[i, j].GetOppositeDanger(bot.Team);
        sum -= Map[i, j].GetDanger(bot.Team);
      }
    }

    if (sum < 0)
      sum = 0f;

    return sum;
  }

  public bool InRange(Point p)
  {
    return InRange(p.X, p.Y);
  }

  public bool InRange(int x, int y)
  {
    return x >= 0 && y >= 0 && x < TileWidth && y < TileHeight;
  }

  public Point ToTile(Vector3 position)
  {
    var result = (position + new Vector3(Width/2f, 0, Height/2f)) / TileSize;
    return new Point(Mathf.RoundToInt(result.x), Mathf.RoundToInt(result.z));
  }

  public Vector3 ToPositino(Point point)
  {
    return (new Vector3(point.X, 0, point.Y) * TileSize) - new Vector3(Width / 2f, 0, Height / 2f);
  }



  void OnDrawGizmos()
  {
    if (ToDisplay == TeamId.Undefined)
      return;

    for (int i = 0; i < TileWidth; i++)
    {
      for (int j = 0; j < TileHeight; j++)
      {
        var danger = ToDisplay == TeamId.A ? Map[i, j].DangerTeamA : Map[i, j].DangerTeamB;

        if(ToDisplay == TeamId.All)
          Gizmos.color = new Color(Map[i, j].DangerTeamB, 0, Map[i, j].DangerTeamA, 0.5f);
        else
          Gizmos.color = new Color(danger,danger,danger,danger);

        Gizmos.DrawCube(ToPositino(new Point(i,j)), new Vector3(TileSize,TileSize,TileSize));
      }
    }
  }

  public bool IsSolid(int x, int y)
  {
    var world = ToPositino(new Point(x, y));
    NavMeshHit hit;
    var solid = NavMesh.SamplePosition(world, out hit, 0.5f, int.MaxValue);
    return !solid;
  }
}
