﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
  private Bot _target;

  // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	  if (_target == null) return;

	  var direction = (_target.transform.position - transform.position).normalized;
	  transform.position += direction * 10f*Time.deltaTime;
	}

  public void SetTarget(Bot closestBot)
  {
    _target = closestBot;
  }
}
